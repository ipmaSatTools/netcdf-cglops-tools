"""Functional tests for netcdfcustomizer module"""

from __future__ import division
import pytest

from netcdf_cglops_tools import netcdfcustomizer
from netcdf_cglops_tools import netcdfanalyzer


pytestmark = pytest.mark.functional


def test_extract_region_of_interest_lst(lst_nc_path, tmpdir):
    first_lon = -5
    last_lon = 10
    first_lat = -5
    last_lat = 10
    output_path = netcdfcustomizer.extract_region_of_interest(
        lst_nc_path,
        first_lon=first_lon,
        first_lat=first_lat,
        last_lon=last_lon,
        last_lat=last_lat,
        output_dir=str(tmpdir),
    )
    output_info = netcdfanalyzer.ProductInfo(output_path)
    bbox = output_info.edge_bounding_box
    _assert_float_equal(bbox.upper_left_lon, first_lon)
    _assert_float_equal(bbox.upper_left_lat, last_lat)
    _assert_float_equal(bbox.lower_right_lon, last_lon)
    _assert_float_equal(bbox.lower_right_lat, first_lat)


def test_extract_variables_lst(lst_nc_path, tmpdir):
    variables = ["LST", "Q_FLAGS"]
    output_path = netcdfcustomizer.extract_variables(
        lst_nc_path,
        variables,
        output_dir=str(tmpdir)
    )
    output_info = netcdfanalyzer.ProductInfo(output_path)
    assert output_info.variables.keys() == variables


def test_extract_region_of_interest_lst10(lst10_nc_path, tmpdir):
    first_lon = -5
    last_lon = 10
    first_lat = -5
    last_lat = 10
    output_path = netcdfcustomizer.extract_region_of_interest(
        lst10_nc_path,
        first_lon=first_lon,
        first_lat=first_lat,
        last_lon=last_lon,
        last_lat=last_lat,
        output_dir=str(tmpdir),
    )
    output_info = netcdfanalyzer.ProductInfo(output_path)
    bbox = output_info.edge_bounding_box
    _assert_float_equal(bbox.upper_left_lon, first_lon)
    _assert_float_equal(bbox.upper_left_lat, last_lat)
    _assert_float_equal(bbox.lower_right_lon, last_lon)
    _assert_float_equal(bbox.lower_right_lat, first_lat)


def test_extract_variables_lst10(lst10_nc_path, tmpdir):
    variables = ["MAX"]
    output_path = netcdfcustomizer.extract_variables(
        lst10_nc_path,
        variables,
        output_dir=str(tmpdir)
    )
    output_info = netcdfanalyzer.ProductInfo(output_path)
    assert output_info.variables.keys() == variables


def _assert_float_equal(float_a, float_b, threshold=0.1):
    """An assertion for comparing floats for equality"""
    assert abs(float_a - float_b) < threshold
