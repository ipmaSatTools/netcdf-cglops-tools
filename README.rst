netcdfcustomizer
================

This repository has tools for working with Copernicus GL's NetCDF products that
are generated at IPMA


Installation
------------

This package depends on:

* NetCDF4 - should not need to be installed, the corresponding python
  package is distributed in pypi as a wheel (AKA precompiled binary package).
* cdo - make sure to install it previously by running `apt-get install cdo`

Get the code by git cloning and then run

    pip3 install -r requirements/production.txt
    pip3 install -e .

The provided `Dockerfile` provides ready to use containers with the project
already installed.


Testing
-------

Use the provided Dockerfile to build an image and then run a container, install
the `dev` dependencies and use `py.test` to run tests

    docker run \
      --rm \
      -ti \
      --entrypoint /bin/bash \
      <image-name> \
      -c "pip install --user -r requirements/dev.txt && py.test"


Development
-----------

Run a container, mount the code as a volume and use a non privileged user:

    docker run \
    --rm \
    -ti \
    --volume <host_path_into_code>:/usr/src/app \
    <image> \
    /bin/sh

    # inside the container
    pip install --user -r requirements/dev.txt
    ipython


Extra - Installing CDO
----------------------

If your distribution does not provide a cdo package with version 1.7.0 or
greater you'll need to compile cdo.

* Download the most recent *stable* CDO release (currently v1.7.0) from:

  https://code.zmaw.de/projects/cdo/files

* Extract to a working directory
* Run the *configure* script with something like the following:

     CDO_INSTALL_PREFIX=$HOME/apps

     ./configure --prefix=${CDO_INSTALL_PREFIX} --with-netcdf=yes \
         --with-hdf5=yes --with-proj=yes --with-curl=yes --with-fftw3=yes \
         --with-libxml=yes

  If the configuration fails, make sure you install the *-dev* packages of the
  libraries that CDO depends on.

* Compile and install CDO

     make
     make install

